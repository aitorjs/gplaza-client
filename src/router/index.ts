import { createRouter, createWebHashHistory } from "vue-router";
import NecesidadesView from "../views/NecesidadesView.vue";
console.log("route base", import.meta.env.BASE_URL)
const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: "/",
      name: "home",
      redirect: '/Donosti/necesidades'
    },
    {
      path: "/:community/necesidades",
      name: "necesidades",
      component: NecesidadesView,
    },
    {
      path: "/:community/dones",
      name: "dones",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/DonesView.vue"),
    },
  ],
});

export default router;
