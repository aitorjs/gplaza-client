# CHANGELOG

## v0.0.1 (09/09/2022) Proof of concept

- Using jquery, bootstrap, express and mongo.

## v0.0.2 (xx/xx/2022)

- Start change from jquery to vuejs. Allmost done.
- Refactor of the express HTTP API.
- Added migrator script that migrates from ods (libreoffice/openoffice calc) to mongo database.
